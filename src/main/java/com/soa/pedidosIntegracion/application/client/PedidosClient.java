package com.soa.pedidosIntegracion.application.client;


import com.soa.pedidosIntegracion.application.model.GuardarPedido;
import com.soa.pedidosIntegracion.application.model.ObtenerPedido;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Repository
@FeignClient(name = "pedidos-api", url = "${pedidos-api.url}")
public interface PedidosClient {

    @PostMapping("/pedido")
    ResponseEntity<GuardarPedido> guardarPedido(
            @RequestBody GuardarPedido guardarPedido
    );

    @GetMapping("/pedido")
    ResponseEntity<List<ObtenerPedido>> consultarPedido(
            @RequestParam(value="parametro")  String parametro,
            @SpringQueryMap ObtenerPedido parametros
    );

    @GetMapping("/pedido/{pedidoId}")
    ResponseEntity<ObtenerPedido> obtenerPedido(
            @PathVariable("pedidoId") Integer pedidoId
    );

}
