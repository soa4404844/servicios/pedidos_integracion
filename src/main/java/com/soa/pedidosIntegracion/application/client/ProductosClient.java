package com.soa.pedidosIntegracion.application.client;


import com.soa.pedidosIntegracion.application.model.ObtenerProducto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
@FeignClient(name = "productos-api", url = "${productos-api.url}")
public interface ProductosClient {

    @GetMapping("/producto/{productoId}/stock")
    ResponseEntity<ObtenerProducto> verificarStockProducto(
            @PathVariable("productoId") Integer productoId,
            @RequestParam(value="cantidad") Double cantidad);

    @PutMapping("/producto/{productoId}")
    ResponseEntity<ObtenerProducto> actualizarProducto(
            @PathVariable("productoId") Integer productoId,
            @SpringQueryMap ObtenerProducto parametros
    );
}
