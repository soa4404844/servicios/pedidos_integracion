package com.soa.pedidosIntegracion.application.client;


import com.soa.pedidosIntegracion.application.model.GuardarPedidoProducto;
import com.soa.pedidosIntegracion.application.model.ObtenerPedidoProducto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
@FeignClient(name = "pedidos-productos-api", url = "${pedidos-productos-api.url}")
public interface PedidosProductosClient {

    @PostMapping("/producto")
    ResponseEntity<List<GuardarPedidoProducto>> guardarPedidoProductos(
            @RequestBody List<GuardarPedidoProducto> guardarPedidoProducto
    );

    @GetMapping("/producto")
    ResponseEntity<List<ObtenerPedidoProducto>> obtenerPedidoProductos(
            @RequestParam(value="parametro")  String parametro,
            @SpringQueryMap ObtenerPedidoProducto parametros
    );
}
