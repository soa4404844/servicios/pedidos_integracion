package com.soa.pedidosIntegracion.application.client;


import com.soa.pedidosIntegracion.application.model.ObtenerOperador;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Repository
@FeignClient(name = "rrhh-api", url = "${rrhh-api.url}")
public interface OperadoresClient {

    @GetMapping("/operador/{operadorID}")
    ResponseEntity<ObtenerOperador> obtenerOperador(
            @PathVariable("operadorID") Integer operadorID
    );
}
