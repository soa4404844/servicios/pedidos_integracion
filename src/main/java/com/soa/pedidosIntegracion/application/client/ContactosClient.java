package com.soa.pedidosIntegracion.application.client;


import com.soa.pedidosIntegracion.application.model.GuardarContacto;
import com.soa.pedidosIntegracion.application.model.ObtenerContacto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Repository
@FeignClient(name = "contactos-api", url = "${contactos-api.url}")
public interface ContactosClient {

    @PostMapping(value = "/contacto", consumes = "application/json")
    ResponseEntity<GuardarContacto> guardarContacto(
            @RequestBody GuardarContacto guardarContacto
    );

    @GetMapping("/contacto/{contactoId}")
    ResponseEntity<ObtenerContacto> obtenercontacto(
            @PathVariable("contactoId") Integer contactoId
    );
}
