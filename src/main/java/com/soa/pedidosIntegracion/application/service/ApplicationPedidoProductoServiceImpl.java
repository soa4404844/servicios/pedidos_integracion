package com.soa.pedidosIntegracion.application.service;


import com.soa.pedidosIntegracion.application.api.IntegracionApiDelegate;
import com.soa.pedidosIntegracion.application.client.*;
import com.soa.pedidosIntegracion.application.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;


@Component
public class ApplicationPedidoProductoServiceImpl implements IntegracionApiDelegate {

    @Autowired
    private PedidosClient pedidosClient;

    @Autowired
    private ContactosClient contactosClient;

    @Autowired
    private PedidosProductosClient pedidosProductosClient;

    @Autowired
    private OperadoresClient operadoresClient;

    @Autowired
    private ProductosClient productosClient;


    @Override
    public ResponseEntity<RegistrarPedido> registrarPedido(RegistrarPedido registrarPedido) {

        System.out.println("Inicio de Registro de Pedido");

        var registrarPedidoBuilder = RegistrarPedido.builder();

        var nuevoContacto = contactosClient.guardarContacto(registrarPedido.getContacto()).getBody();


        if(Objects.requireNonNull(nuevoContacto).getContactoID() != null) {
            System.out.println("Nuevo Contacto Registrado: "+ nuevoContacto.getContactoID());

            registrarPedido.getPedido().setContactoID(nuevoContacto.getContactoID());

            registrarPedidoBuilder = registrarPedidoBuilder.contacto(nuevoContacto);
        }


        var nuevoPedido = pedidosClient.guardarPedido(registrarPedido.getPedido()).getBody();

        if(Objects.requireNonNull(nuevoPedido).getPedidoID() != null) {

            System.out.println("Nuevo Pedido Registrado: "+ nuevoPedido.getPedidoID());

            registrarPedidoBuilder = registrarPedidoBuilder.pedido(nuevoPedido);

            var productosRegistrar = registrarPedido.getProductos().stream().filter(pp -> {

                var producto = productosClient.verificarStockProducto(
                        pp.getProductoID(),
                        pp.getCantidad()
                ).getBody();

                if (Objects.requireNonNull(producto).getProductoID() != null) {
                    return true;
                } else {
                    System.out.println("Producto stock no disponible: " + pp.getProductoID());
                    return false;
                }
            }).map(pp -> {
                pp.setPedidoID(nuevoPedido.getPedidoID());
                return pp;
            }).toList();

            var pedidosProductosRegistrado = pedidosProductosClient.guardarPedidoProductos(productosRegistrar).getBody();

            pedidosProductosRegistrado.forEach(pp -> {
                System.out.println("Pedido Producto registrado: " + pp.getPedidoProductoID());
            });

            registrarPedidoBuilder = registrarPedidoBuilder.productos(pedidosProductosRegistrado);
        }

        return ResponseEntity.ok(
                registrarPedidoBuilder.build()
        );
    }


    @Override
    public ResponseEntity<List<ConsultarPedido>> consultarPedidos(
            String parametro,
            ObtenerPedido parametros
    ) {

        List<ConsultarPedido> consultaPedidos = new ArrayList<>();

        var pedidos = pedidosClient.consultarPedido(
                parametro,
                parametros
        );

        Function<Integer, ObtenerContacto> getContacto = (contactoID) -> {
            var contacto = contactosClient.obtenercontacto(contactoID).getBody();

            return Objects.requireNonNull(contacto).getContactoID() != null
                    ? contacto
                    : ObtenerContacto.builder().build();
        };

        Function<Integer, ObtenerOperador> getOperador = (operadorID) -> {

            if (operadorID != null) {
                var operador = operadoresClient.obtenerOperador(operadorID).getBody();

                return Objects.requireNonNull(operador).getOperadorID() != null
                        ? operador
                        : ObtenerOperador.builder().build();
            }

            return ObtenerOperador.builder().build();
        };

        Function<Integer, List<ObtenerPedidoProducto>> getPedidoProducto = (pedidoID) -> {

            var pedidoProductos = pedidosProductosClient.obtenerPedidoProductos(
                    "",
                    ObtenerPedidoProducto.builder().pedidoID(pedidoID).build()
            ).getBody();

            return !Objects.requireNonNull(pedidoProductos).isEmpty()
                    ? pedidoProductos
                    : new ArrayList<>();
        };

        if (!Objects.requireNonNull(pedidos.getBody()).isEmpty()) {

            consultaPedidos = pedidos.getBody().stream().map(pedido -> ConsultarPedido.builder()
                        .pedido(pedido)
                        .contacto(getContacto.apply(pedido.getContactoID()))
                        .operador(getOperador.apply(pedido.getOperadorID()))
                        .productos(getPedidoProducto.apply(pedido.getPedidoID()))
                        .build()
            ).toList();
        }

        return ResponseEntity.ok(
                consultaPedidos
        );
    }

    @Override
    public ResponseEntity<AsignarPedido> asignarPedido(
            Integer pedidoId,
            AsignarPedido asignarPedido)
    {

        System.out.println("Inicio de asginar pedido : " + pedidoId);

        var asignarPedidoBuilder = AsignarPedido.builder();

        var operadorAsignar = operadoresClient.obtenerOperador(
                asignarPedido.getOperador().getOperadorID()
        ).getBody();

        if (Objects.requireNonNull(operadorAsignar).getOperadorID() != null) {
            asignarPedidoBuilder = asignarPedidoBuilder.operador(operadorAsignar);

            var pedido = pedidosClient.obtenerPedido(pedidoId).getBody();

            System.out.println("Inicio de obtener pedido");

            if (Objects.requireNonNull(pedido).getPedidoID() != null) {

                System.out.println("pedido obtenido: " + pedido.getPedidoID());

                var guardarPedido= GuardarPedido.builder()
                        .pedidoID(pedido.getPedidoID())
                        .ventaID(pedido.getVentaID())
                        .contactoID(pedido.getContactoID())
                        .operadorID(operadorAsignar.getOperadorID())
                        .estado(asignarPedido.getPedido().getEstado())
                        .fechaEntrega(pedido.getFechaEntrega())
                        .observaciones(pedido.getObservaciones())
                        .build();

                guardarPedido = pedidosClient.guardarPedido(guardarPedido).getBody();

                if (Objects.requireNonNull(guardarPedido).getPedidoID() != null) {

                    System.out.println("Pedido Asginado correctamente: " + guardarPedido.getPedidoID());

                    asignarPedidoBuilder = asignarPedidoBuilder.pedido(
                            ObtenerPedido.builder()
                                    .pedidoID(guardarPedido.getPedidoID())
                                    .ventaID(guardarPedido.getVentaID())
                                    .contactoID(guardarPedido.getContactoID())
                                    .operadorID(guardarPedido.getOperadorID())
                                    .estado(guardarPedido.getEstado())
                                    .fechaEntrega(guardarPedido.getFechaEntrega())
                                    .observaciones(guardarPedido.getObservaciones())
                                    .build()
                    );
                }
            }
        } else {
            System.out.println("Operador no existe: " + asignarPedido.getOperador().getOperadorID());
        }
        return ResponseEntity.ok(
                asignarPedidoBuilder.build()
        );
    }

    @Override
    public ResponseEntity<AtenderPedido> atenderPedido(Integer pedidoId){

        System.out.println("Inicio de atender pedido : " + pedidoId);

        var pedioAtendidoBuilder = AtenderPedido.builder();

        var pedido = pedidosClient.obtenerPedido(pedidoId).getBody();

        System.out.println("Inicio de obtener pedido");

        if (Objects.requireNonNull(pedido).getPedidoID() != null) {

            System.out.println("pedido obtenido: " + pedido.getPedidoID());

            if (!pedido.getEstado().equals("ATENDIDO")) {

                var pedidoProductos = pedidosProductosClient.obtenerPedidoProductos(
                        "",
                        ObtenerPedidoProducto.builder()
                                .pedidoID(pedido.getPedidoID())
                                .build()
                ).getBody();

                System.out.println("pedido producto obtenido: " + Objects.requireNonNull(pedidoProductos).size());

                pedidoProductos.forEach(pp -> {
                    var producto = productosClient.verificarStockProducto(
                            pp.getProductoID(),
                            pp.getCantidad()
                    ).getBody();

                    if (Objects.requireNonNull(producto).getProductoID() != null) {

                        var nuevaCantidad = producto.getCantidad() - pp.getCantidad();

                        producto.setCantidad(nuevaCantidad);
                        producto = productosClient.actualizarProducto(
                                pp.getProductoID(),
                                producto
                        ).getBody();

                        if (Objects.requireNonNull(producto).getProductoID() != null) {
                            System.out.println("Producto Inventario actualizado : " + producto.getProductoID());
                        }
                    } else {
                        System.out.println("Producto stock no disponible: " + pp.getProductoID());
                    }
                });

                var guardarPedido = GuardarPedido.builder()
                        .pedidoID(pedido.getPedidoID())
                        .ventaID(pedido.getVentaID())
                        .contactoID(pedido.getContactoID())
                        .operadorID(pedido.getOperadorID())
                        .estado("ATENDIDO")
                        .fechaEntrega(pedido.getFechaEntrega())
                        .observaciones(pedido.getObservaciones())
                        .fechaTransaccion(pedido.getFechaTransaccion())
                        .build();

                guardarPedido = pedidosClient.guardarPedido(guardarPedido).getBody();

                if (Objects.requireNonNull(guardarPedido).getPedidoID() != null) {

                    System.out.println("Pedido Atendido correctamente: " + guardarPedido.getPedidoID());

                    pedioAtendidoBuilder = pedioAtendidoBuilder.pedido(
                            ObtenerPedido.builder()
                                    .pedidoID(guardarPedido.getPedidoID())
                                    .ventaID(guardarPedido.getVentaID())
                                    .contactoID(guardarPedido.getContactoID())
                                    .operadorID(guardarPedido.getOperadorID())
                                    .estado(guardarPedido.getEstado())
                                    .fechaEntrega(guardarPedido.getFechaEntrega())
                                    .observaciones(guardarPedido.getObservaciones())
                                    .fechaTransaccion(guardarPedido.getFechaTransaccion())
                                    .build()
                    );
                }
            }
            else {
                System.out.println("Pedido fue atendido");
                pedioAtendidoBuilder = pedioAtendidoBuilder.pedido(pedido);
            }
        }

        return ResponseEntity.ok(
                pedioAtendidoBuilder.build()
        );
    }


}

