package com.soa.pedidosIntegracion;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.soa.pedidosIntegracion.application.client")
public class PedidosIntegracionConfig {
}
